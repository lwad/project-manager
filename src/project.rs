use skim::prelude::{Skim, SkimItemReader, SkimOptionsBuilder};
use std::{
  fs::{create_dir_all, File},
  io::{BufRead, BufReader, Cursor, Error, Write},
  path::PathBuf,
};

pub struct Project {
  pub name: String,
  pub path: String,
}

pub type Projects = Vec<Project>;

pub fn read_registry(path: &PathBuf) -> Result<Projects, Error> {
  if !path.exists() {
    return Ok(Vec::new());
  }

  let file = File::open(path)?;
  let reader = BufReader::new(file);

  let lines = reader
    .lines()
    .filter_map(|line| {
      let line = line.ok()?;
      let (name, path) = line.split_once(" ")?;
      Some(Project {
        name: name.to_string(),
        path: path.to_string(),
      })
    })
    .collect();

  Ok(lines)
}

pub fn write_registry(path: &PathBuf, projects: &Projects) {
  let lines = projects
    .iter()
    .map(|project| project.name.clone() + " " + &project.path + "\n")
    .collect::<Vec<_>>()
    .join("");

  create_dir_all(
    path
      .parent()
      .expect("Project registry parent directory could not be created."),
  )
  .expect("Project registry parent directory could not be created.");

  let mut file = File::create(path).expect("Project registry could not be created.");
  file
    .write(lines.as_bytes())
    .expect("Projects registry could not be written to.");
}

pub fn project_name_from_path(projects: &Projects, path: &String) -> Option<String> {
  projects
    .iter()
    .find(|project| project.path == *path)
    .map(|project| project.name.clone())
}

pub fn project_path_from_name(projects: &Projects, name: &String) -> Option<String> {
  projects
    .iter()
    .find(|project| project.name == *name)
    .map(|project| project.path.clone())
}

pub fn clean_project_name(project_name: &String) -> String {
  project_name.replace(".", "").replace(":", "")
}

pub fn search_projects(projects: &Projects, initial_query: Option<&str>) -> Option<String> {
  let project_names = projects
    .iter()
    .map(|project| project.name.clone())
    .collect::<Vec<_>>()
    .join("\n");

  let options = SkimOptionsBuilder::default()
    .height(Some("100%"))
    .multi(false)
    .query(initial_query)
    .select1(true)
    .build()
    .unwrap();

  let items = SkimItemReader::default().of_bufread(Cursor::new(project_names));

  let result = Skim::run_with(&options, Some(items)).expect("No skim result.");

  if result.is_abort {
    None
  } else {
    result
      .selected_items
      .first()
      .map(|item| item.output().to_string())
  }
}

// Delete a project if it exists.
pub fn delete_project(projects: &mut Projects, project_name: &str) {
  let index = projects
    .iter()
    .take_while(|project| project.name != *project_name)
    .count();
  if index < projects.len() {
    projects.remove(index);
  }
}

// Add a project.
pub fn add_project(projects: &mut Projects, project_name: &str, project_path: &str) {
  // Delete the project if it exists.
  delete_project(projects, project_name);
  // Add the project.
  projects.insert(
    projects.len(),
    Project {
      name: project_name.to_string(),
      path: project_path.to_string(),
    },
  );
}
