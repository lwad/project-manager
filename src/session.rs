use tmux_interface::{
  commands::tmux::StdIO, AttachSession, HasSession, KillSession, ListClients, NewSession,
  SwitchClient, Tmux,
};

pub fn get_tmux_variable(expression: &str) -> Option<String> {
  Tmux::with_command(
    ListClients::new()
      .target_session(std::env::var("TMUX_PANE").ok()?)
      .format(expression),
  )
  .output()
  .ok()
  .map(|output| output.to_string().trim().to_string())
}

pub fn session_exists(session_name: &str) -> bool {
  Tmux::with_command(HasSession::new().target_session(session_name))
    .stderr(Some(StdIO::Null))
    .status()
    .unwrap()
    .success()
}

pub fn create_session(session_name: &str, session_path: &str) {
  let _ = Tmux::with_command(
    NewSession::new()
      .session_name(session_name)
      .start_directory(session_path)
      .detached(),
  )
  .status()
  .unwrap()
  .success();
}

pub fn focus_session(session_name: &str) {
  match std::env::var("TMUX").as_deref() {
    Err(_) | Ok("ignore") => Tmux::with_command(AttachSession::new().target_session(session_name))
      .status()
      .unwrap()
      .success(),
    Ok(_) => Tmux::with_command(SwitchClient::new().target_session(session_name))
      .status()
      .unwrap()
      .success(),
  };
}

pub fn kill_session(session_name: &str) {
  let _ = Tmux::with_command(KillSession::new().target_session(session_name))
    .stderr(Some(StdIO::Null))
    .status()
    .unwrap()
    .success();
}
