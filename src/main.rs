mod project;
mod session;

use project::*;
use session::*;
use std::{
  env::{args, var},
  path::PathBuf,
};

#[derive(PartialEq)]
enum Mode {
  Add,
  Delete,
  Base,
  Find,
}

// Get application mode from the first argument.
fn get_mode(arg: Option<&String>) -> Mode {
  match arg.map(|arg| arg.as_str()) {
    Some("--add") => Mode::Add,
    Some("--delete") => Mode::Delete,
    Some("--base") => Mode::Base,
    _ => Mode::Find,
  }
}

// Get the path of the project registry.
fn get_registry_path() -> PathBuf {
  let paths = [
    ("PROJECT_MANAGER_PATH", ""),
    ("XDG_DATA_HOME", "/projects"),
    ("HOME", "/projects"),
  ];

  let path = paths
    .iter()
    .find(|(base, _)| var(base).is_ok())
    .map(|(base, relative)| var(base).unwrap().to_string() + relative)
    .unwrap_or("/tmp/projects".to_string());

  PathBuf::from(path)
}

// Get the value of an argument given a prefix.
fn get_arg_value<'a>(args: &'a Vec<String>, arg_name: &str) -> Option<&'a String> {
  let mut arg = None;
  for arg_index in 2..args.len() {
    if args.get(arg_index - 1).unwrap().as_str() == arg_name {
      arg = args.get(arg_index);
    }
  }
  arg
}

// Get the canonical version of a path.
fn canonicalise_path_string(path_string: String) -> String {
  // Attempt to get the canonical path buffer.
  let path_buf = PathBuf::from(&path_string).canonicalize();

  match path_buf {
    // Convert the canonical path buffer into a string.
    Ok(canon_path) => canon_path.to_str().unwrap().to_string(),
    // Change nothing of the input string.
    Err(_) => path_string,
  }
}

// Operate in base mode.
fn base_mode() {
  // If the session doesn't exist, create it.
  if !session_exists("base") {
    create_session("base", "~");
  }

  // Focus the session.
  focus_session("base");
}

// Operate in delete mode.
fn delete_mode(args: Vec<String>) {
  let registry_path = get_registry_path();
  let mut projects = read_registry(&registry_path).expect("The registry could not be read.");

  // Get the name of the project from the program arguments or reverse lookup via the Tmux "pane_start_path" variable.
  let project_name = get_arg_value(&args, "--name").map(clean_project_name);
  let project_name = project_name.or_else(|| {
    let prev_session_path = get_tmux_variable("#{pane_start_path}").unwrap_or(String::from("~"));
    project_name_from_path(&projects, &prev_session_path)
  });

  // If the project can be found, remove it from the registry.
  if let Some(ref project_name) = project_name {
    delete_project(&mut projects, project_name);
    write_registry(&registry_path, &projects);
  }

  // Focus the base session, creating it first if necessary.
  base_mode();

  // Kill any associated session if required and the project could be found.
  if args.contains(&String::from("--kill")) {
    if let Some(project_name) = project_name {
      if session_exists(&project_name) {
        kill_session(&project_name);
      }
    }
  }
}

// Operate in add mode.
fn add_mode(args: Vec<String>) {
  let registry_path = get_registry_path();
  let mut projects = read_registry(&registry_path).expect("The registry could not be read.");

  // Get the path of the project from the program arguments or the Tmux "pane_start_path" variable.
  let project_path = get_arg_value(&args, "--path").map(|string| string.to_owned());
  let project_path = project_path.or_else(|| get_tmux_variable("#{pane_start_path}"));

  // Use the canonical project path (expanded/absolute).
  let project_path = project_path.map(canonicalise_path_string);

  // Get the name of the project from the program arguments or the basename of the project path (if found).
  let project_name = get_arg_value(&args, "--name").map(String::from);
  let project_name = project_name.or_else(|| {
    ((&project_path).clone())?
      .split("/")
      .last()
      .map(|string| string.to_string())
  });
  let project_name = project_name.as_ref().map(clean_project_name);

  // If a project name and path could be found...
  if let (Some(ref project_name), Some(ref project_path)) =
    (project_name.clone(), project_path.clone())
  {
    // Add the project to the registry.
    add_project(&mut projects, project_name, project_path);
    write_registry(&registry_path, &projects);

    // If a session associated with the project name doesn't already exist, create it.
    if !session_exists(project_name) {
      create_session(project_name, project_path);
    }

    // Focus the session.
    focus_session(project_name);
  }
}

// Operate in find mode.
fn find_mode(args: Vec<String>) {
  let registry_path = get_registry_path();
  let projects = read_registry(&registry_path).expect("The registry could not be read.");

  // Get the name of the project from the program arguments.
  let project_name = search_projects(&projects, args.get(1).map(|string| string.as_str()));

  // Get the path of the project from a lookup by the project name.
  let project_path = project_name
    .clone()
    .and_then(|project_name| project_path_from_name(&projects, &project_name));

  // If the project could be found, use its name as the session name.
  // Otherwise, if the previous session is associated with a project, use the name of the previous session.
  // Otherwise, use the name of the base session.
  let session_name = match project_name {
    Some(project_name) => project_name,
    None => {
      let prev_session_path = get_tmux_variable("#{pane_start_path}").unwrap_or(String::from("~"));
      project_name_from_path(&projects, &prev_session_path).unwrap_or("base".to_string())
    }
  };

  // If a session associated with the project doesn't already exist, create it.
  if !session_exists(&session_name) {
    create_session(&session_name, &project_path.unwrap_or("~".to_string()));
  }

  // Focus the project's session.
  focus_session(&session_name);
}

fn main() {
  let args: Vec<String> = args().collect();
  let mode = get_mode(args.get(1));

  match mode {
    Mode::Add => add_mode(args),
    Mode::Delete => delete_mode(args),
    Mode::Base => base_mode(),
    Mode::Find => find_mode(args),
  }

  // Kill the PROJECTS session.
  if session_exists("PROJECTS") {
    kill_session("PROJECTS");
  }
}
