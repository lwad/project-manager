{ rustPlatform, tmux }:

rustPlatform.buildRustPackage {
  pname = "project-manager";
  version = "2.0.3";

  src = ./.;

  cargoLock.lockFile = ./Cargo.lock;

  buildInputs = [ tmux ];

  meta = {
    description = "A project manager using tmux sessions written in Rust.";
    homepage = "https://git.disroot.org/lwad/project-manager";
  };
}
