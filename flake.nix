{
  description = "A tmux session navigator written in Rust.";
  inputs.nixpkgs.url = "nixpkgs";
  outputs = { self, nixpkgs }: {
    nixosModules.default = { config, lib, pkgs, ... }: {
      options.project-manager = {
        useAlias = lib.mkEnableOption "alias from p to project-manager";
        enable = lib.mkEnableOption "project-manager";
        store = lib.mkOption {
          default = null;
          description =
            "The location of the file in which to store project information.";
          type = with lib.types; uniq (nullOr str);
        };
        useBinds =
          lib.mkEnableOption "tmux keybindings specific to project-manager";
      };
      config = let
        cfg = config.project-manager;
        inherit (lib) mkIf optional;
      in {
        environment = {
          sessionVariables.PROJECT_MANAGER_PATH =
            mkIf (cfg.store != null) cfg.store;
          shellAliases.p =
            mkIf cfg.useAlias "${pkgs.project-manager}/bin/project-manager";
          systemPackages = optional cfg.enable pkgs.project-manager;
        };
        nixpkgs.overlays =
          [ (_: _: { project-manager = pkgs.callPackage ./default.nix { }; }) ];
        programs.tmux.enable = mkIf cfg.enable true;
        programs.tmux.extraConfig =
          let pm = "${pkgs.project-manager}/bin/project-manager";
          in mkIf cfg.useBinds ''
            bind -n m-f new-session -s "PROJECTS" -c "#{pane_start_path}" " ${pm}"
            bind -n m-b new-session -s "PROJECTS" " ${pm} --base"
            bind -n m-a new-session -s "PROJECTS" -c "#{pane_current_path}" " ${pm} --add"
            bind -n m-d new-session -s "PROJECTS" -c "#{pane_start_path}" " ${pm} --delete"
          '';
      };
    };
    packages = nixpkgs.lib.mapAttrs
      (system: pkgs: { default = pkgs.callPackage ./default.nix { }; })
      nixpkgs.legacyPackages;
  };
}
