# project-manager

`project-manager` is a tmux session navigation utility for active projects.

It keeps a resgistry of projects (name and directory) on which sessions are
based, creating them on-demand.

## Usage

`project-manager --add [--path <project_path>] [--name <project_name>]` creates
a new project. The default directory is the cwd, and the default project name
is the directory's basename.

`project-manager --delete [--kill] [--name <project_name>]` deletes the target
project and navigates to the "base" session. If `--kill` is provided, the
session associated with the project is killed if it exists. The default target
is the project of the current session.

`project-manager --base` navigates to the "base" session.

`project-manager [<initial_search>]` opens a fzf-like search interface in which
selecting a project navigates to its session.

### Bash Interactive Shell Initialisation

Bash users can insert this at the start of their bashrc to automatically enter
the base session when starting an interactive shell.

```bash
if test -z "$TMUX"; then
  timeout=$((EPOCHSECONDS+2))
  if ! tmux attach-session -t "base" 2>/dev/null && test "$EPOCHSECONDS" -lt "$timeout"; then
    tmux new-session -s "base"
  fi
  exit 0
fi
```

_The timeout is to prevent recreating the base session after killing it with a
non-zero exit code._

## Installation

The program can be built with `cargo build --release` or `nix build`.

### NixOS Module

The NixOS module provides the following options:

| Name                       | Description                                                |
| -------------------------- | ---------------------------------------------------------- |
| `project-manager.enable`   | Adds project-manager to PATH and enables tmux.             |
| `project-manager.store`    | Location of the project resgistry.                         |
| `project-manager.useBind`  | Binds `alt+{a\|d\|b\|f}` for add/delete/base/find in tmux. |
| `project-manager.useAlias` | Aliases `p` to `project-manager`.                          |

If you don't want it in your path and you already have tmux enabled, you can
reference it as `${pkgs.project-manager}` without enabling it.

To use the module:

```nix
# flake.nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    # 1. Add project-manager as a flake input.
    project-manager = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "git+https://git.disroot.org/lwad/project-manager.git";
    };
  };
  outputs = { nixpkgs, ... }@inputs: {
    nixosConfigurations.yourHostname = nixpkgs.lib.nixosSystem {
      # 2. Add the module from the input to your modules list.
      modules = [
        inputs.project-manager.nixosModules.default
      ];
    };
  };
}
```

[A full reference of module use.](https://git.disroot.org/lwad/nixos.git)
